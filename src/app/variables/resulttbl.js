export const medicines = [
     {
          "CATMAT": "437869",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "FUMARATO DE DIMETILA, 120MG, LIBERAÇÃO CONTROLADA",
          "Nº processo": 30276,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmB4jpdGjBY/6b197e3a34c996d053e54c3beb319151fa561764?#tarefa-131199"
     },
     {
          "CATMAT": "270846",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS",
          "Insumo": "ESTRADIOL, VALERATO ASSOCIADO COM NORETISTERONA ENANTATO, 5MG + 50MG/1ML, INJETÁVEL",
          "Nº processo": 30312,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7zyzp4blB0/6c28e00d37001c09640c9e8e568bc224bb5cb2a1?#tarefa-142201"
     },
     {
          "CATMAT": "336505",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUANTITATIVO DE ANTI SARAMPO VÍRUS IGG, ELISA, TESTE",
          "Nº processo": 30223,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Pesquisa",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm0Z2RdGjBY/c20c0b6ce7d1a77c6e713ec7db4ba96e447d5007"
     },
     {
          "CATMAT": "266820",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IMUNOGLOBULINA HUMANA, ENDOVENOSA, 5 G, PÓ PARA SOLUÇÃO INJETÁVEL",
          "Nº processo": 30069,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "ARP",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               10,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/Orml2qLJx6P1z5Xo/8785737f849a009c1a5c8ac0216d2e79a2819b98?#tarefa-133361"
     },
     {
          "CATMAT": "0358132",
          "Depto.": "Analista/COBIES/DASI/SESAI/MS",
          "Insumo": "FRALDA DESCARTÁVEL, ANATÔMICO, GRANDE, ACIMA DE 70 KG, FLOCOS DE GEL, ABAS ANTIVAZAMENTO, FAIXA AJUSTÁVEL, FITAS ADESIVAS MULTIAJUSTÁVEIS, REUTILIZÁVEIS, ALGODÃO NÃO DESFAÇA QUANDO MOLHADO",
          "Nº processo": 30044,
          "CIM.": "",
          "Área demandante": "COBIES/DASI/SESAI/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/Bd6Gbpg5jORDjQOV/a945b0812ec716c23f36c23ecaaa0e64d85e4de4?#tarefa-138461"
     },
     {
          "CATMAT": "453061",
          "Depto.": "Coordenador Geral/CGLICI/CONJUR/MS",
          "Insumo": "VACINA, INFLUENZA TRIVALENTE, FRAGMENTADA, INATIVADA, SUSPENSÃO INJETÁVEL",
          "Nº processo": 30225,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Minuta",
          "Status atual": "CONJUR",
          "Status geral": [
               13,
               5,
               6
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGezPoyAZz/7dad9ec2a3e61f0b18ac6ce01ef049b4b2717398?#tarefa-92915"
     },
     {
          "CATMAT": "437156",
          "Depto.": "Analista/CGAFME/DAF/SCTIE/MS, Coordenador Geral/CGAFME/DAF/SCTIE/MS",
          "Insumo": "HIPOCLORITO DE SÓDIO, SOLUÇÃO AQUOSA, ATÉ 2,5% DE CLORO ATIVO",
          "Nº processo": 30299,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73BEP4blB0/f3fa8ff6a8948d16a8ee52b72ea44976eb6d3bce?#tarefa-126019"
     },
     {
          "CATMAT": "436704",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BUPRENORFINA, 5 MG, ADESIVO TRANSDÉRMICO",
          "Nº processo": 30249,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 20,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mxJqEwAbv/d823c2b89c49110d7de47004cdd3538318c21c07?#tarefa-101262"
     },
     {
          "CATMAT": "349794",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "ALFAGALSIDASE, CONCENTRAÇÃO 1, FORMA FARMACÊUTICA, SOLUÇÃO PARA INFUSÃO",
          "Nº processo": 30087,
          "CIM.": "CIM",
          "Área demandante": "CGJUD",
          "Risco": "R0",
          "Modalidade": "Inexigibilidade CGJUD - SCTIE",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               11,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/OD6o1RZn89Rjbzdm/e800266b1efb3bce9d75dad8621a02ed923e390f?#tarefa-115717"
     },
     {
          "CATMAT": "308805",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CONCENTRADO DE FATOR DE COAGULAÇÃO, FATOR IX, AE = OU > 50UI, PÓ LIÓFILO P/ INJETÁVEL",
          "Nº processo": 30110,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Pregão",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               7,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/8J9K6qbyyERjeNVY/d48a7524d0e72e5aec399ebeeb11aae2dda3ef5c?#tarefa-141367"
     },
     {
          "CATMAT": "308805",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CONCENTRADO DE FATOR DE COAGULAÇÃO, FATOR IX, AE = OU > 50UI, PÓ LIÓFILO P/ INJETÁVEL",
          "Nº processo": 30110,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Pregão",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               7,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/8J9K6qbyyERjeNVY/d48a7524d0e72e5aec399ebeeb11aae2dda3ef5c?#tarefa-141367"
     },
     {
          "CATMAT": "276388",
          "Depto.": "Chefe/DICONF/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "GALANTAMINA, SAL BROMIDRATO, 8MG",
          "Nº processo": 30133,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               13,
               12,
               13
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vg2pEwAbv/0dd64dd89ef69f72809849e6f49def31985297dd?#tarefa-35178"
     },
     {
          "CATMAT": "434872",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TOFACITINIBE, SAL CITRATO, 5MG",
          "Nº processo": 30152,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               13,
               12,
               13
          ]
     },
     {
          "CATMAT": "410008",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "MULTIVITAMINAS, ASSOCIADAS COM SAIS MINERAIS, VITS: A, B1,B2,B6,B12,C,D E E, MINERAIS: FERRO, ZINCO, SELÊNIO, COBRE E IODO, PÓ ORAL",
          "Nº processo": 30154,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vn2pEwAbv/e286f9eb1792ae28e729f1ab4d61036c17f6a352?#tarefa-58465"
     },
     {
          "CATMAT": "410008",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "MULTIVITAMINAS, ASSOCIADAS COM SAIS MINERAIS, VITS: A, B1,B2,B6,B12,C,D E E, MINERAIS: FERRO, ZINCO, SELÊNIO, COBRE E IODO, PÓ ORAL",
          "Nº processo": 30154,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vn2pEwAbv/e286f9eb1792ae28e729f1ab4d61036c17f6a352?#tarefa-58465"
     },
     {
          "CATMAT": "428080",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "RALTEGRAVIR, 100 MG",
          "Nº processo": 30164,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               13,
               12,
               13
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WYdPzBDrQ/682f10505f39200fd8557b07627837b41ac068c5?#tarefa-66114"
     },
     {
          "CATMAT": "412094",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "FINGOLIMODE CLORIDRATO, 0,5 MG",
          "Nº processo": 30339,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R0",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Ratificação Inex. / Disp.",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               1,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7zgEp4blB0/726eba622355111566d201f6ce5e0afa5ac27998?#tarefa-130647"
     },
     {
          "CATMAT": "436739",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO,, CONJUNTO COMPLETO, QUANTITATIVO ANTI ZIKA VÍRUS IGG, ELISA, TESTE",
          "Nº processo": 30233,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGJKPoyAZz/4091751bd5c7be57d76176ea1bac728333df2f24"
     },
     {
          "CATMAT": "436329",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO,, CONJUNTO COMPLETO, QUANTITATIVO DE ANTI CHIKUNGUNYA VÍRUS IGG, ELISA, TESTE",
          "Nº processo": 30235,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mr2qEwAbv/d039afd62ba4b07fe4bd63c5e87f90c9b548f503?#tarefa-141219"
     },
     {
          "CATMAT": "0380127",
          "Depto.": "Coordenador/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "LARVICIDA PIRIPROXIFENO",
          "Nº processo": 30245,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm0aXRdGjBY/255d0f4e64de401b04dbd9055d5874838066cc06?#tarefa-99752"
     },
     {
          "CATMAT": "336500",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUANTITATIVO DE ANTI DENGUE VÍRUS IGM, ELISA, TESTE",
          "Nº processo": 30262,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Preço",
          "Status geral": [
               13,
               5,
               6
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDjgRoyAZz/aa550d65d6bb40029ddad1e4e5beacc34c875e0f?#tarefa-106597"
     },
     {
          "CATMAT": "442435",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DELTAMETRINA, 1 G, EM COLEIRA, USO VETERINÁRIO",
          "Nº processo": 30020,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R1",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 11,
          "Status anterior": "Pregão",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               7,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/AwKaDqoZjMR8nBly/073731b42fbc19cfd7443d7cda71bbd9c06da4a7?#tarefa-141464"
     },
     {
          "CATMAT": "272789",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "LEVONORGESTREL, ASSOCIADO À ETINILESTRADIOL, 0,15MG + 0,03MG, BLISTER CALENDÁRIO COM 21 COMPRIMIDOS",
          "Nº processo": 30028,
          "CIM.": "CIM",
          "Área demandante": "CGSM/DAPES/SAS/MS",
          "Risco": "R1",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 62,
          "Status anterior": "Homologação",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               8,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/OD6o1RZK9bqjbzdm/9445daed74653b211d4153a6c6613d2d7fd2b04e?#tarefa-143859"
     },
     {
          "CATMAT": "5487",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ANALISE CLINICA, ANATOMIA PATOLOGICA E CITOPATOLOGIA",
          "Nº processo": 30127,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R1",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 18,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7E6Oq4blB0/a9225bfbe873bfc058686adef76d00ce5f2ddaca?#tarefa-41756"
     },
     {
          "CATMAT": "414430",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "GOLIMUMABE, 50 MG, SOLUÇÃO INJETÁVEL, EM SERINGA PREENCHIDA",
          "Nº processo": 30130,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R1",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               13,
               12,
               13
          ]
     },
     {
          "CATMAT": "270436",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "METOTREXATO 2,5MG",
          "Nº processo": 30161,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R1",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               5,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqymzZqoyAZz/75050139a5f17341653f1322f18cec7c16100e10?#tarefa-143199"
     },
     {
          "CATMAT": "352933",
          "Depto.": "Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "LEVETIRACETAM, 100MG/ML, SOLUÇÃO ORAL",
          "Nº processo": 30265,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R1",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               5,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBkXpdGjBY/fa5fce94443b7093ab4a88d902fc5d0b64d5e872?#tarefa-107722"
     },
     {
          "CATMAT": "419075",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "OLIGONUCLEOTÍDEOS, REAÇÃO DE PCR, ESPECIALMENTE PREPARADO, ESCALA 250 NMOL",
          "Nº processo": 30081,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               5,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/NdAMXP2A2jpe87JQ/5002d7f395eb11cad2990ee316854773124f2ceb?#tarefa-141698"
     },
     {
          "CATMAT": "272787",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ALFADORNASE 1MG/ML, SOLUÇÃO PAR INALAÇÃO",
          "Nº processo": 30115,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": "Contrato",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               13,
               0
          ]
     },
     {
          "CATMAT": "271154",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "INSULINA, HUMANA, REGULAR, 100U/ML, INJETÁVEL",
          "Nº processo": 30124,
          "CIM.": "CIM",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Homologação",
          "Status atual": "Ratificação Preço",
          "Status geral": [
               13,
               8,
               6
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8ywQqzBDrQ/7c3fe4189211023bd892d450da56b2d99da498e7?#tarefa-136327"
     },
     {
          "CATMAT": "268174",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DESMOPRESSINA ACETATO 0,1MG/ML",
          "Nº processo": 30129,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "CONJUR",
          "Status atual": "Ratificação Preço",
          "Status geral": [
               13,
               4,
               6
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8y19qzBDrQ/30d5741a75ac67c998eb61e5e60ecb4fae6d094f?#tarefa-143618"
     },
     {
          "CATMAT": "439541",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA, INATIVADA, VÍRUS RÁBICO, CEPA PV, SUSPENSÃO INJETÁVEL, USO VETERINÁRIO",
          "Nº processo": 30141,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 62,
          "Status anterior": "Pesquisa",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqya97qoyAZz/7aeccae304e2ee68af51ec6e534222e5dd317430?#tarefa-46115"
     },
     {
          "CATMAT": "268395",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ANFOTERICINA B, 50 MG, INJETÁVEL",
          "Nº processo": 30143,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 36,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vGYpEwAbv/166d7cdab5fc9cfb3ccb1c26dedd4e2250c12524?#tarefa-84281"
     },
     {
          "CATMAT": "272653",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ENTACAPONA 200MG",
          "Nº processo": 30148,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8y0KqzBDrQ/68b97f34daf736349c51ea834a5631fbf4a5610e?#tarefa-54113"
     },
     {
          "CATMAT": "271154",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "INSULINA, HUMANA, REGULAR, 100U/ML, INJETÁVEL",
          "Nº processo": 30189,
          "CIM.": "CIM",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "R2",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 76,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmOJjPdGjBY/057a8f7fe5907cf29f0f86f430e455387d64c845"
     },
     {
          "CATMAT": "439541",
          "Depto.": "Analista/DIVEO/CGORF/DLOG/SE/MS",
          "Insumo": "VACINA, INATIVADA, VÍRUS RÁBICO, CEPA PV, SUSPENSÃO INJETÁVEL, USO VETERINÁRIO",
          "Nº processo": 30208,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R2",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Autorização Pós",
          "Status geral": [
               13,
               7,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGmvPoyAZz/6ae1b9d23fc7643f7c2fc72fc59433b023099a21?#tarefa-83635"
     },
     {
          "CATMAT": "408449",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "OLIGONUCLEOTÍDEOS, REAÇÃO DE PCR, ESPECIALMENTE PREPARADO, ESCALA 100 NMOL",
          "Nº processo": 30074,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Pesquisa",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               1,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/ldGVzPeaeYPZ4aoW/fda690fe9668196cff173d5aec3cbe50a6c6d202?#tarefa-141338"
     },
     {
          "CATMAT": "272094",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ZIDOVUDINA, ASSOCIADA COM LAMIVUDINA, 300MG + 150MG",
          "Nº processo": 30086,
          "CIM.": "CIM",
          "Área demandante": "CGAE/DIAHV/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Contrato",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               13,
               0
          ]
     },
     {
          "CATMAT": "408449",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "OLIGONUCLEOTÍDEOS, REAÇÃO DE PCR, ESPECIALMENTE PREPARADO, ESCALA 100 NMOL",
          "Nº processo": 30092,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7MKEq4blB0/abb4eab7662e55867cbbba00ba9a787f6bc3980f?#tarefa-141200"
     },
     {
          "CATMAT": "408449",
          "Depto.": "Chefe/DICONF/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "OLIGONUCLEOTÍDEOS, REAÇÃO DE PCR, ESPECIALMENTE PREPARADO, ESCALA 100 NMOL",
          "Nº processo": 30093,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/Gx5YvROjEmPQKMmk/836adf45af2ca1ac30512c48646c012866e881c0?#tarefa-41955"
     },
     {
          "CATMAT": "354228",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "OLIGONUCLEOTÍDEOS, REAÇÃO DE PCR, ESPECIALMENTE PREPARADO, ESCALA 200 NMOL",
          "Nº processo": 30099,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               6,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmeQyqdGjBY/7192642af2ed840afaae400525a36a2fe4847a66?#tarefa-41956"
     },
     {
          "CATMAT": "268317",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ABACAVIR SULFATO, 20 MG/ML, SOLUÇÃO ORAL",
          "Nº processo": 30109,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 17,
          "Status anterior": "Contrato",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               13,
               0
          ]
     },
     {
          "CATMAT": "266627",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ALFAEPOETINA HUMANA RECOMBINANTE, SOLUÇÃO INJETÁVEL, 1.000 UI",
          "Nº processo": 30125,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               6,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5v0apEwAbv/b52f1a8ce299b2addc88b65aba428ff5ad40b1d9?#tarefa-143838"
     },
     {
          "CATMAT": "268381",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "AMICACINA SULFATO, 250 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30135,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 38,
          "Status anterior": "CONJUR",
          "Status atual": "Lista de Verificação",
          "Status geral": [
               13,
               3,
               5
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmNmwpdGjBY/1a244736d46122fcaa625c05217fec7f73b99166?#tarefa-141459"
     },
     {
          "CATMAT": "272083",
          "Depto.": "Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "SEVELAMER, CLORIDRATO, 800 MG",
          "Nº processo": 30136,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 41,
          "Status anterior": "Pregão",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               7,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyalvqoyAZz/70fbc11b5d7451decc96ba837e61194bcbd4ef5d?#tarefa-118081"
     },
     {
          "CATMAT": "362057",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "RIVASTIGMINA - ADESIVO TRANSDÉRMICO 9 MG",
          "Nº processo": 30146,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmND2pdGjBY/2c5816f8dce29c8a681a1868f5b7989e418db3a2?#tarefa-51033"
     },
     {
          "CATMAT": "268118",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "FILGRASTIM, 300MCG, INJETÁVEL",
          "Nº processo": 30147,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               5,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqya6zqoyAZz/ae1668e6ef01259c9ed3eea8cb4850a5ac0b98b2?#tarefa-51613"
     },
     {
          "CATMAT": "359135",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "DASATINIBE, 20 MG",
          "Nº processo": 30150,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Negociação",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               1,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7ED5q4blB0/f997a1e7b83f812e61a9c2374ba50b46a8cfbb07?#tarefa-143238"
     },
     {
          "CATMAT": "304788",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CINACALCETE, 30 MG",
          "Nº processo": 30151,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vzapEwAbv/979d55da5528728f1f9441d21ef9a6a96fe17b30?#tarefa-54173"
     },
     {
          "CATMAT": "400476",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "PARICALCITOL, 5 MCG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30153,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7EYGq4blB0/e3a6568ae19437d7bea84eb84860b7383b155c81?#tarefa-57619"
     },
     {
          "CATMAT": "325838",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DEFERASIROX 125 MG",
          "Nº processo": 30157,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqym5vqoyAZz/7cf4d22872debba23b9bbc1a54b3ce92007e45bd"
     },
     {
          "CATMAT": "434765",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "ABATACEPTE, 125 MG/ML, SOLUÇÃO INJETÁVEL, SERINGA PREENCHIDA",
          "Nº processo": 30169,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 55,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WD9PzBDrQ/3b3d331f9e0250cd2613ec3e652c147d6dbc9486?#tarefa-69155"
     },
     {
          "CATMAT": "383272",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "IMIGLUCERASE, 400 UI, PÓ LIÓFILO P/INJETÁVEL",
          "Nº processo": 30172,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 18,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmOMwPdGjBY/7359d8ef2e62bcb4c4e2b5356a3cfdd80f01e54b?#tarefa-71311"
     },
     {
          "CATMAT": "430630",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TERIFLUNOMIDA 14MG",
          "Nº processo": 30184,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 42,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WzlPzBDrQ/3f6c15ebd0a3655544ad24ae0d458d5cbfeeb6f6?#tarefa-75439"
     },
     {
          "CATMAT": "431599",
          "Depto.": "Coordenador/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "SOFOSBUVIR, 400 MG",
          "Nº processo": 30222,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7m9wq4blB0/701aa19a444528b040d13e461a40955cf4f734b8?#tarefa-133341"
     },
     {
          "CATMAT": "428423",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TENOFOVIR, ASSOCIADA À LAMIVUDINA, 300 MG + 300 MG",
          "Nº processo": 30226,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8OEKPzBDrQ/7549b1e95a81ec083f3eeac25cf6b4fe83e73ab1?#tarefa-93054"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "PRESERVATIVO MASCULINO, BORRACHA NAT., COMP. MÍN .DE 160 MM, LARGURA NOMINAL 52 MM, ESPESSURA MÍN. 0,03MM, LUBRIFICADO, S/ ESPERMICIDA ,S/ ODOR, C/ RESERVATÓRIO, ",
          "Nº processo": 30227,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R3",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGNrPoyAZz/1f8b709ec144b34bef085b30ff9e8d4ce8c183ca?#tarefa-93614"
     },
     {
          "CATMAT": "417811",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TAFAMIDIS 20MG",
          "Nº processo": 30271,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R3",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDl7RoyAZz/1c526edfdbf32e9a1563b14cafd7d7cbf756efa6?#tarefa-109431"
     },
     {
          "CATMAT": "356044",
          "Depto.": "Pregoeiro/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO, QUANTITATIVO DE COLINESTERASE, COLORIMÉTRICO, TESTE",
          "Nº processo": 30001,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": "Homologação",
          "Status atual": "Pregão",
          "Status geral": [
               14,
               7,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/AwKaDqoZEYR8nBly/0a1ca33324eeaba598d9cd041e448d9d04a9a191?#tarefa-42130"
     },
     {
          "CATMAT": "0278264",
          "Depto.": "Pregoeiro/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TRIENTINA CLORIDRATO",
          "Nº processo": 30029,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 13,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/Bd6Gbpg52rRDjQOV/99b0f4b90fee7ff8ce839d326172acd72376a05f?#tarefa-141902"
     },
     {
          "CATMAT": "435222",
          "Depto.": "Pregoeiro/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "INSETICIDA, TIPO PIRETRÓIDE, IMPREGNADA EM REDE DE POLIÉSTER, P/ REDE, CERCA DE 200 X 80 X 200 CM, C/ MANGAS",
          "Nº processo": 30039,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 17,
          "Status anterior": "IRP",
          "Status atual": "Pregão",
          "Status geral": [
               13,
               7,
               8
          ],
          "href": "http://localhost:8089/processos/visualizar/AwKaDqoZzOR8nBly/6233a6bc0bfe636c8ffe512a2ced811bc8144484"
     },
     {
          "CATMAT": "389779",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "RASAGILINA 1MG",
          "Nº processo": 30040,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 42,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               13,
               12,
               13
          ]
     },
     {
          "CATMAT": "278338",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ÁCIDO TRANEXÂMICO, 250 MG",
          "Nº processo": 30058,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Contrato",
          "Status geral": [
               12,
               11,
               12
          ],
          "href": "http://localhost:8089/processos/visualizar/oaKQdq3lYApWnJNL/9da6124c336341693a2b8dbede681d0e8e8b65a8?#tarefa-60767"
     },
     {
          "CATMAT": "287481",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "NICOTINA, 2 MG, GOMA DE MASCAR",
          "Nº processo": 30066,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Homologação",
          "Status atual": "IRP",
          "Status geral": [
               13,
               8,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/vzw3MREBLmqg0LKy/8c75cf49e9d884c78349f96e8fa6ce85141d6843?#tarefa-142203"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "REAGENTE  DIAGNÓSTICO CLÍNICO, CONJ  AUTOMAÇÃO, CULTURA BACTERIANA, TESTE, C/MEIO MIDDLEBROOK 7H9 E SOLUÇÃO DE ENRIQUECIMENTO",
          "Nº processo": 30078,
          "CIM.": "CIM",
          "Área demandante": "CGLAB/DEGEVS/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 112,
          "Status anterior": "Homologação",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               8,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/wa8lop0OKjq1eVzv/0a550577809d6c1aa70c4812e5a363b574e5c3ac?#tarefa-70816"
     },
     {
          "CATMAT": "0287252",
          "Depto.": "Analista/CGAFME/DAF/SCTIE/MS",
          "Insumo": "NICOTINA 2MG",
          "Nº processo": 30083,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 46,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/MnL0gRMwkXp4Wx5a/a9929e9a4c619b77181929efda5dd0601bb0718f?#tarefa-132239"
     },
     {
          "CATMAT": "267674",
          "Depto.": "Coordenador/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "HIDROCLOROTIAZIDA, 25 MG",
          "Nº processo": 30097,
          "CIM.": "CIM",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               10,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5bx4pEwAbv/6190e5a634855d4e5b1b2316d09cd524481e1a86?#tarefa-139138"
     },
     {
          "CATMAT": "268573",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DESMOPRESSINA ACETATO 0,1MG",
          "Nº processo": 30103,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BEevDPJYmbp2zmko/627379556accff628ddd83a993eda43932240f6b?#tarefa-37685"
     },
     {
          "CATMAT": "270616",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BENZILPENICILINA, POTÁSSICA, 5.000.000UI, INJETÁVEL",
          "Nº processo": 30105,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 36,
          "Status anterior": "Homologação",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               8,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmN5XpdGjBY/a361cd7bf4b1158a6d4ce5b8e09ba9b6aa295bd6?#tarefa-143499"
     },
     {
          "CATMAT": "399500",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ARMADILHA ANIMAL, LUMINOSA CDC, LUZ INCADESCENTE, MOTOR 6 VOLTS, HÉLICE 4 PÁS, VEN, PLÁSTICO RESISTENTE, ROTAÇÃO ANTI-HORÁRIO, SUPORTE AÇO INOX, TAMPA PRO",
          "Nº processo": 30112,
          "CIM.": "CIM",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": "Homologação",
          "Status atual": "ARP",
          "Status geral": [
               13,
               9,
               10
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8yWdqzBDrQ/2e2474811c008d5f41b9c7923a6ee70bb205142a?#tarefa-42556"
     },
     {
          "CATMAT": "308802",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CONCENTRADO DE FATOR DE COAGULAÇÃO, FATOR VIII, AE = OU > 100UI, PÓ LIÓFILO P/ INJETÁVEL",
          "Nº processo": 30126,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 36,
          "Status anterior": "Pregão",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               7,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmNbgpdGjBY/94429320e5d84127dcb610d13b7972e44a7bdb4c?#tarefa-86321"
     },
     {
          "CATMAT": "308802",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CONCENTRADO DE FATOR DE COAGULAÇÃO, FATOR VIII, AE = OU > 100UI, PÓ LIÓFILO P/ INJETÁVEL",
          "Nº processo": 30126,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 36,
          "Status anterior": "Pregão",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               7,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmNbgpdGjBY/94429320e5d84127dcb610d13b7972e44a7bdb4c?#tarefa-86321"
     },
     {
          "CATMAT": "332383",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IDURSULFASE, 2MG/ML, SOLUÇÃO P/ INFUSÃO VENOSA",
          "Nº processo": 30142,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vrJpEwAbv/40e6e8ab3a178109486b8e3517640bf91be6784e?#tarefa-47611"
     },
     {
          "CATMAT": "272785",
          "Depto.": "Chefe/DICONF/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DONEPEZILA, 5 MG",
          "Nº processo": 30144,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Ratificação Preço",
          "Status atual": "IRP",
          "Status geral": [
               13,
               5,
               7
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7EAwq4blB0/077d384fd6a0254b5c30708e456c2cc19060156a?#tarefa-49941"
     },
     {
          "CATMAT": "272972",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "FENOXIMETILPENICILINA, POTÁSSICA, 80.000UI/ML, SOLUÇÃO ORAL",
          "Nº processo": 30149,
          "CIM.": "CIM",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 46,
          "Status anterior": "Pesquisa",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5vDvpEwAbv/98b8236f0f23617c1b8075d04399416da02bf13d?#tarefa-52293"
     },
     {
          "CATMAT": "369176",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "EVEROLIMO 0,50 MG",
          "Nº processo": 30155,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 20,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WZ0PzBDrQ/2d8860d9344b15b59682e843a4719f17af0a4643?#tarefa-143438"
     },
     {
          "CATMAT": "330537",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "GALSULFASE 5MG/5ML",
          "Nº processo": 30158,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 75,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WaaPzBDrQ/66a7fabe425d98a08ed2092376925adde2655081?#tarefa-109614"
     },
     {
          "CATMAT": "268439",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CLARITROMICINA, 500 MG",
          "Nº processo": 30166,
          "CIM.": "CIM",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 40,
          "Status anterior": "IRP",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               6,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5BaapEwAbv/a08fd55c97fd334570de59a16980135cf4fbec22?#tarefa-68172"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TIPRANAVIR, 250 MG ",
          "Nº processo": 30167,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Pré-Empenho",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               4,
               5
          ],
          "href": "http://localhost:8089/processos/visualizar/QykBOpDz40RVL60A/b306f35e27eb22f5a420600d25a8a178593f0ebb?#tarefa-68313"
     },
     {
          "CATMAT": "343607",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS, Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "ETANERCEPTE, 25 MG, PÓ LIÓFILO P/ INJETÁVEL, COM KIT DE APLICAÇÃO",
          "Nº processo": 30168,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 126,
          "Status anterior": "Nenhum",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               0,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7NgOp4blB0/3e3845fdfc3d28e88798faedd98ecb81984e9787"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS, Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "SOMATROPINA, SOMATOTROFINA HUMANA RECOMBINANTE, 4UI, INJETÁVEL ",
          "Nº processo": 30170,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 125,
          "Status anterior": "Nenhum",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               0,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5BwGpEwAbv/3e92ddc868ff22dc2bd928cde33fbcc159ca9c4b"
     },
     {
          "CATMAT": "290058",
          "Depto.": "Coordenador Geral/CGCIS/SCTIE/MS",
          "Insumo": "ADALIMUMABE, 40 MG, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30174,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 62,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmO8yPdGjBY/bb8bb3247240192672204023aa57a3009bfada74?#tarefa-101418"
     },
     {
          "CATMAT": "446524",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "LANREOTIDA ACETATO, 120MG/ML, SOLUÇÃO INJETÁVEL, LIBERAÇÃO PROLONGADA - SERINGA 0,5ML",
          "Nº processo": 30175,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7Nr9p4blB0/6da60ada9ec0131da19c076b7fc1c69bf708a32a?#tarefa-72621"
     },
     {
          "CATMAT": "273317",
          "Depto.": "Chefe/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IMATINIBE MESILATO, 100 MG",
          "Nº processo": 30182,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "IRP",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               5,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7N8Jp4blB0/fff19f952b6266df3c8c36cd06c76c4de8fc0fea?#tarefa-75259"
     },
     {
          "CATMAT": "428790",
          "Depto.": "Diretor/DAF/SCTIE/MS",
          "Insumo": "ELOSULFASE ALFA, CONCENTRAÇÃO 1, FORMA FARMACÊUTICA SOLUÇÃO INJETÁVEL",
          "Nº processo": 30191,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8WldPzBDrQ/14b7cb2e46406ed060222b7680c37ad372179dfb?#tarefa-129160"
     },
     {
          "CATMAT": "448579",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "MICOFENOLATO, MOFETILA, 500MG",
          "Nº processo": 30199,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 47,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mMaqEwAbv/51b5168c818c1fb05e7459ff8120b2c9a0d7d6b9?#tarefa-78048"
     },
     {
          "CATMAT": "442073",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "INTERFERONA, BETA 1A, 60MCG/ML, SOLUÇÃO INJETÁVEL, SERINGA PREENCHIDA C/ SISTEMA DE APLICAÇÃO ¿ SERINGA 0,5ML",
          "Nº processo": 30220,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mdYqEwAbv/370c2ce4b2827af78d1b369e62cfd77dc2037e8b?#tarefa-133698"
     },
     {
          "CATMAT": "367664",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "NATALIZUMABE, 20 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30224,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7m7Vq4blB0/bd502165e64c4affe44d38eeaf0e4247b4f364b8?#tarefa-133206"
     },
     {
          "CATMAT": "5487",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "ANALISE CLINICA, ANATOMIA PATOLOGICA E CITOPATOLOGIA",
          "Nº processo": 30228,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8O3QPzBDrQ/8ab3b15e7d6d3f45bc0cdbb54163a641f583c9b8?#tarefa-93842"
     },
     {
          "CATMAT": "380545",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "LOPINAVIR, ASSOCIADO COM RITONAVIR, 100 MG + 25 MG",
          "Nº processo": 30234,
          "CIM.": "CIM",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 13,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8Og4PzBDrQ/ee85e155de7356fb03206889061c899cbcca0479?#tarefa-143958"
     },
     {
          "CATMAT": "345001",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "MIGLUSTATE, 100 MG",
          "Nº processo": 30254,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8GW9PzBDrQ/360053bb6a638dbd0b51e1bd605b7fe019df1fc8?#tarefa-103851"
     },
     {
          "CATMAT": "374967",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "NILOTINIBE, 200 MG",
          "Nº processo": 30278,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8G8dPzBDrQ/cc5b6b496c917750d488b023f4c9a19fe9cff388?#tarefa-113065"
     },
     {
          "CATMAT": "0292373",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "TOXINA BOTULÍNICA TIPO A 500UI INJETÁVEL",
          "Nº processo": 30279,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBG2pdGjBY/9f66cdf33c2c1a178bf5267c4a2add44b34d527e?#tarefa-114273"
     },
     {
          "CATMAT": "272429",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS, Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "CLOZAPINA, 25 MG",
          "Nº processo": 30280,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 35,
          "Status anterior": "Nenhum",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               0,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73GVP4blB0/41012f44e53651d6d2961f811532c3245dfa21b5?#tarefa-114752"
     },
     {
          "CATMAT": "267896",
          "Depto.": "Pregoeiro/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "RIVASTIGMINA, 1,5 MG",
          "Nº processo": 30292,
          "CIM.": "CIM",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "R4",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyD6KRoyAZz/623239d757f8ac96bc027862e82f968b5f993a29?#tarefa-117609"
     },
     {
          "CATMAT": "0464614",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA, SARAMPO E RUBÉULA, ATENUADA, PÓ LIÓFILO P/ INJETÁVEL + DILUENTE.",
          "Nº processo": 30327,
          "CIM.": "Sim",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "Risco 0",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7zz5p4blB0/4ee7094604f693bbffd4bad8095778b80e8e3770?#tarefa-126205"
     },
     {
          "CATMAT": "267140",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "AZITROMICINA, 500 MG",
          "Nº processo": 30054,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Fiscal",
          "Status atual": "ARP",
          "Status geral": [
               13,
               10,
               10
          ],
          "href": "http://localhost:8089/processos/visualizar/wa8lop0OLXq1eVzv/2d5a41b173b4cb5ab9c162c10391257c3876594c?#tarefa-141142"
     },
     {
          "CATMAT": "0315056",
          "Depto.": "Secretário/SESAI/MS",
          "Insumo": "ÁGUA DESTILADA, BIDESTILADA, ESTÉRIL, APIROGÊNICA",
          "Nº processo": 30016,
          "CIM.": "",
          "Área demandante": "COBIES/DASI/SESAI/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/oaKQdq3vLARWnJNL/8be7a64d2234a5d042fbdb5c5711052b3b99d8ad?#tarefa-140841"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Coordenador Geral/CGSH/DAET/SAES/MS",
          "Insumo": "Hemoderivados Plasma",
          "Nº processo": 30018,
          "CIM.": "",
          "Área demandante": "CGSH/DAET/SAES/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/Bd6Gbpg5oLRDjQOV/6540be153a5b320842047efebce4a2b913024863?#tarefa-42690"
     },
     {
          "CATMAT": "269587",
          "Depto.": "Analista/COBIES/DASI/SESAI/MS",
          "Insumo": "COMPRESSA GAZE, TECIDO 100% ALGODÃO, 13 FIOS/CM2, COR BRANCA,ISENTA DE IMPUREZAS, 8 CAMADAS, 7,50 CM, 7,50 CM, 5 DOBRAS, C/ FIO RADIOPACO,ESTÉRIL,DESCARTÁVEL",
          "Nº processo": 30043,
          "CIM.": "",
          "Área demandante": "COBIES/DASI/SESAI/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/OD6o1RZKd9qjbzdm/b902b9e2d2e92ffa3fb3d387282b1c23a650212f?#tarefa-138323"
     },
     {
          "CATMAT": "0312616",
          "Depto.": "Analista/COBIES/DASI/SESAI/MS",
          "Insumo": "LENÇOL DESCARTÁVEL, PAPEL, 0,50 M, 70M, ROLO, MACA HOSPITALAR",
          "Nº processo": 30045,
          "CIM.": "",
          "Área demandante": "COBIES/DASI/SESAI/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/wa8lop069jR1eVzv/7c805478a1a738a6eb4ff39d7be18f53cd4d82a7?#tarefa-138447"
     },
     {
          "CATMAT": "280201",
          "Depto.": "Secretário/SAES/MS",
          "Insumo": "BORTEZOMIBE 3,5MG",
          "Nº processo": 30055,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Pregão CGJUD",
          "Dias Corridos (Ult. Tarefa)": 292,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/r8JBOpKYrVRleL42/8de817d2a146c7960372612c15d975c1a6e832dd?#tarefa-2981"
     },
     {
          "CATMAT": "455315",
          "Depto.": "Analista/CGAE/DAET/SAES/MS",
          "Insumo": "PRÓTESE DE QUADRIL, APLICAÇÃO CIRURGIA PRIMÁRIA, ESTRURURA NÃO MODULAR, MATERIAL AÇO INOXIDÁVEL, COMPONENTE HASTE C/ CABEÇA FIXA, TIPO FIXAÇÃO CIMENTADA, MODELO TIPO THOMPSON",
          "Nº processo": 30108,
          "CIM.": "",
          "Área demandante": "CGAE/DAET/SAES/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 157,
          "Status anterior": "Pesquisa",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               1,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7EWzq4blB0/0b51eb71442ec7e0d113802e0254a68eb00635d7?#tarefa-58239"
     },
     {
          "CATMAT": "453064",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA DIFTERIA, TÉTANO, PERTUSSIS , HEPATITE B, POLIOMIELITE 1,2 E 3, H. INFLUENZAE B, PÓ LIÓFILO P/ INJETÁVEL + DILUENTE.",
          "Nº processo": 30160,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Pesquisa",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               1,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5BBEpEwAbv/2334c34b46ade3c8d49c379450c5befbed8b9246?#tarefa-142662"
     },
     {
          "CATMAT": "48518",
          "Depto.": "Secretário/SAES/MS",
          "Insumo": "AMBULÂNCIA, AMBULANCIA",
          "Nº processo": 30163,
          "CIM.": "",
          "Área demandante": "CGURG/DAHU/SAES/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 123,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqymQ4qoyAZz/c4a36af5182d55b28f50887457d86ca142ed2e16?#tarefa-65748"
     },
     {
          "CATMAT": "434378",
          "Depto.": "Analista/CGJUD",
          "Insumo": "MACITENTANO 10MG",
          "Nº processo": 30177,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 96,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqymJgqoyAZz/bfa66e9e21ef1fae71901ecbdcd37130a378e19b?#tarefa-76042"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "USTEQUINUMABE 90MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30181,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 14,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmOxrPdGjBY/8f0dad9348ee4b9fe63b9dfb9b0f704572d71fa1?#tarefa-121878"
     },
     {
          "CATMAT": "453816",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 33,33MG/ML, SOLUÇÃO ORAL ¿ GOTAS; 30ML",
          "Nº processo": 30193,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 95,
          "Status anterior": "Doc. Téc.e Fiscal",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               2,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5BnvpEwAbv/edca0411a3932012ee19567371f0e6ba6c3c501e?#tarefa-140318"
     },
     {
          "CATMAT": "452935",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 200MG/ML, SOLUÇÃO ORAL",
          "Nº processo": 30196,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 17,
          "Status anterior": "Ratificação Inex. / Disp.",
          "Status atual": "Publicação",
          "Status geral": [
               13,
               9,
               10
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7NJ5p4blB0/3406e3a0cfb8524fff32d9ef05a11ddb1903bd4a?#tarefa-140578"
     },
     {
          "CATMAT": "454125",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, CANABIDIOL (CBD), 1000MG, BÁLSAMO, ISENTO THC; 118ML",
          "Nº processo": 30197,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 13,
          "Status anterior": "Doc. Téc.e Fiscal",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               2,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqym4rqoyAZz/21a12fb35e043e6dbc4c6c05b14d802825d12576?#tarefa-140303"
     },
     {
          "CATMAT": "460924",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TOBRAMICINA, 300MG, SOLUÇÃO PARA INALAÇÃO",
          "Nº processo": 30201,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Pesquisa",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGW6PoyAZz/3512b436e10c39b3c770fa8e0752c1c7943b8df9?#tarefa-79580"
     },
     {
          "CATMAT": "439252",
          "Depto.": "Analista/CGJUD, Coordenador Geral/CGJUD",
          "Insumo": "NUSINERSENA, CONCENTRAÇÃO 2,4, FORMA FARMACÊUTICA SOLUÇÃO INJETÁVEL",
          "Nº processo": 30202,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 97,
          "Status anterior": "Nenhum",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               0,
               0
          ]
     },
     {
          "CATMAT": "0285965",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "LEVETIRACETAM 250MG",
          "Nº processo": 30204,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Pesquisa",
          "Status atual": "Minuta",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7m5Gq4blB0/f3a392f35c71cf331269fa96ead04709712a2cac?#tarefa-80580"
     },
     {
          "CATMAT": "453071",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA, MENINGOCÓCICA C, CONJUGADA, INJETÁVEL",
          "Nº processo": 30205,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7mWBq4blB0/2e07ce7f789834154938e19633d8f1dd92a8826b?#tarefa-81589"
     },
     {
          "CATMAT": "434378",
          "Depto.": "Analista/CGJUD",
          "Insumo": "MACITENTANO 10MG",
          "Nº processo": 30207,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 82,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm0NwRdGjBY/656d1bddbc782e94a63178fae58ffef75a59cd24?#tarefa-83358"
     },
     {
          "CATMAT": "461066",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "ALFA-ASFOTASE, 40MG/ML, SOLUÇÃO INFETÁVEL C/ 0,7ML",
          "Nº processo": 30211,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Inex. / Disp.",
          "Status geral": [
               13,
               8,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mLNqEwAbv/6cebd739c35136eb9875e8f490287c5a5df8eb84?#tarefa-137958"
     },
     {
          "CATMAT": "448985",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IMUNOGLOBULINA HUMANA, ANTITETÂNICA, 250 UI/ML, SOLUÇÃO INJETÁVEL, SERINGA PREENCHIDA",
          "Nº processo": 30212,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 17,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm0VXRdGjBY/4e4e64d375d03e70dc22c7083b9842f98a44142d?#tarefa-86778"
     },
     {
          "CATMAT": "337473",
          "Depto.": "Analista/DIIMP/CGLOG/DLOG/SE/MS",
          "Insumo": "INSULINA, DETEMIR, 100U/ML, SOLUÇÃO INJETÁVEL 3ML",
          "Nº processo": 30213,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Fiscal",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               12,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7ml0q4blB0/f75d415e5858a71f3f3c3cbf3f6959c5033da42c?#tarefa-94652"
     },
     {
          "CATMAT": "898198",
          "Depto.": "Analista/CGJUD, Coordenador Geral/CGJUD",
          "Insumo": "SEBELIPASE, ALFA, 2MG/ML, SOLUÇÃO INFETÁVEL - FRASCO COM 10ML",
          "Nº processo": 30214,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": "Fiscal",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               12,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8O7VPzBDrQ/ec438313dd294788baaae16b16d436f2a123dda5?#tarefa-95402"
     },
     {
          "CATMAT": "452935",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 200MG/ML, SOLUÇÃO ORAL",
          "Nº processo": 30215,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 40,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Inex. / Disp.",
          "Status geral": [
               13,
               8,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGb7PoyAZz/789817773a90d4bc4fa2100d14cfeda0ff58a4f8?#tarefa-128420"
     },
     {
          "CATMAT": "452935",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 200MG/ML, SOLUÇÃO ORAL",
          "Nº processo": 30216,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 40,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Inex. / Disp.",
          "Status geral": [
               13,
               8,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8O1lPzBDrQ/a89c8fcc330302e2b2d3133e9d0b0244926ce9c1?#tarefa-128421"
     },
     {
          "CATMAT": "454936",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 50MG/ML, USO ORAL, ISENTO DE THC; 120ML",
          "Nº processo": 30217,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 40,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Inex. / Disp.",
          "Status geral": [
               13,
               8,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm09vRdGjBY/c604b3ed781f6f94ad61baa6b488c40fb023522d?#tarefa-128425"
     },
     {
          "CATMAT": "0268158",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "PIRIMETAMINA 25 MG",
          "Nº processo": 30218,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 39,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7mnzq4blB0/e6d522720e7924b899e25b407edfb7988110d02f?#tarefa-143479"
     },
     {
          "CATMAT": "269460",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ÁCIDO URSODESOXICÓLICO, 150MG",
          "Nº processo": 30219,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8OxyPzBDrQ/c5f67beb8970629b388aea88aa725c0c6460e37e?#tarefa-90313"
     },
     {
          "CATMAT": "270120",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "CLONAZEPAM, 2,5 MG/ML, SOLUÇÃO ORAL- GOTAS",
          "Nº processo": 30229,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mXaqEwAbv/c5bf706ecc23367a06392c420b2037d8795c837a?#tarefa-141901"
     },
     {
          "CATMAT": "453086",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA, RAIVA (CULTIVADO EM CÉLULAS VERO), INATIVADA, PÓ LIÓFILO P/ INJETÁVEL + DILUENTE",
          "Nº processo": 30231,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5mYGqEwAbv/fdc0615e26eb8152cbaa31e6228ef242669b0af3?#tarefa-94529"
     },
     {
          "CATMAT": "0343494",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ESPIRAMICINA 1.500.000 UI",
          "Nº processo": 30236,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 28,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5m9LqEwAbv/99a79ce1c55b4280381ad6174c6d1d86116b13cc?#tarefa-97238"
     },
     {
          "CATMAT": "271620",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "OLANZAPINA, 5 MG",
          "Nº processo": 30237,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqm01wRdGjBY/37f744c03d9bcfe46fb5ef10fb3914833a562bf5?#tarefa-97710"
     },
     {
          "CATMAT": "277649",
          "Depto.": "Analista/CGJUD, Coordenador Geral/CGJUD",
          "Insumo": "ANAGRELIDA CLORIDRATO 0,5MG",
          "Nº processo": 30238,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Nenhum",
          "Status atual": "Contrato",
          "Status geral": [
               18,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7m8Eq4blB0/40f0e92b0747e127e5ceae3b296f4510cccae1de?#tarefa-97781"
     },
     {
          "CATMAT": "458859",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BUROSUMABE, 20 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30240,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7mV9q4blB0/579e3e027c89ceae70a3a479d00516f4e7bb7b56?#tarefa-98800"
     },
     {
          "CATMAT": "417568",
          "Depto.": "Chefe/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ÁCIDO QUENODESOXICÓLICO",
          "Nº processo": 30241,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Doc. Téc.e Fiscal",
          "Status atual": "Pré-Empenho",
          "Status geral": [
               13,
               3,
               4
          ],
          "href": "http://localhost:8089/processos/visualizar/Gx5YvROwaARQKMmk/9d0d11f138571e3ac60961c3585617597c0336bf?#tarefa-98807"
     },
     {
          "CATMAT": "417568",
          "Depto.": "Analista/CGJUD",
          "Insumo": "ÁCIDO QUENODESOXICÓLICO",
          "Nº processo": 30242,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Doc. Téc.e Fiscal",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               2,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/OD6o1RZjVgRjbzdm/145dd9d27629e7a9402f9f70b26adaf59ceb786e?#tarefa-140778"
     },
     {
          "CATMAT": "434617",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 14-24%, USO ORAL, SERINGA PREENCHIDA",
          "Nº processo": 30243,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyG6gPoyAZz/bb07706a83969f21fcf9fe876815de73d9073c77?#tarefa-98926"
     },
     {
          "CATMAT": "434617",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 14-24%, USO ORAL, SERINGA PREENCHIDA",
          "Nº processo": 30244,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 45,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8OB1PzBDrQ/2619a39b8155e350f1a1358f61fb2b01a2a594cc?#tarefa-98986"
     },
     {
          "CATMAT": "454125",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, CANABIDIOL (CBD), 1000MG, BÁLSAMO, ISENTO THC; 118ML",
          "Nº processo": 30248,
          "CIM.": "Não",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 13,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyGL7PoyAZz/836eb8bd872009cafe92cb61a3ae804740b14219?#tarefa-140199"
     },
     {
          "CATMAT": "268994",
          "Depto.": "Analista/CGAFME/DAF/SCTIE/MS",
          "Insumo": "BUPROPIONA CLORIDRATO, 150 MG",
          "Nº processo": 30250,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 18,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmB5jpdGjBY/fee6242b7dacbe56d0f38c0bb718580d1115ce2e?#tarefa-137918"
     },
     {
          "CATMAT": "450161",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IVACAFTOR, ASSOCIADO AO LUMACAFTOR 125MG + 200MG",
          "Nº processo": 30251,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 38,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBegpdGjBY/9773c9062f9c0312c463ca44e5e8a1461c2484f3?#tarefa-103523"
     },
     {
          "CATMAT": "448290",
          "Depto.": "Coordenador Geral/CGIES/DLOG/SE/MS",
          "Insumo": "IVACAFTOR 150MG",
          "Nº processo": 30252,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 19,
          "Status anterior": "Lista de Verificação",
          "Status atual": "Ratificação Inex. / Disp.",
          "Status geral": [
               13,
               8,
               9
          ],
          "href": "http://localhost:8089/processos/visualizar/NdAMXP2gAYRe87JQ/f5d847dec4d186ed398f2f455b8f0eae879efe5f?#tarefa-128468"
     },
     {
          "CATMAT": "431633",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO,, CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUALITATIVO ANTI PARVOVÍRUS B19 IGG, QUIMIOLUMINESCÊNCIA, TESTE",
          "Nº processo": 30253,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDa6RoyAZz/261287d83fcbf64627641205896863865b78b7e1?#tarefa-143899"
     },
     {
          "CATMAT": "305270",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "LEVOFLOXACINO, 500 MG",
          "Nº processo": 30255,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 9,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56BGpEwAbv/a2f80dd980b1c898f855af5230037aedcfc5486e?#tarefa-103889"
     },
     {
          "CATMAT": "449377",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "ETEPLIRSEN 50MG/ML, SOLUÇÃO INJETÁVEL C/ 2ML",
          "Nº processo": 30256,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDXbRoyAZz/81cb626fce321e288d0861fb2d14dfcf998de365?#tarefa-137854"
     },
     {
          "CATMAT": "272846",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "TALIDOMIDA, 100 MG",
          "Nº processo": 30257,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq560LpEwAbv/31de78161da957753bce6d16b60962d24cf72dce?#tarefa-133381"
     },
     {
          "CATMAT": "336503",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUANTITATIVO DE ANTI RUBÉOLA VÍRUS IGG, ELISA, TESTE",
          "Nº processo": 30258,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 17,
          "Status anterior": " Validação TR",
          "Status atual": "Pesquisa",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBbwpdGjBY/4e04c56464d70336c9707c6b299e929da3005f0a?#tarefa-143918"
     },
     {
          "CATMAT": "435951",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "AFLIBERCEPTE 40 MG/ML, SOLUÇÃO INJETÁVEL, C/ SISTEMA DE APLICAÇÃO 165MCL",
          "Nº processo": 30259,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP736EP4blB0/9eb71fbe591e85c930e7afde251a6790cd814f15?#tarefa-142861"
     },
     {
          "CATMAT": "411872",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA HUMANA, PNEUMOCÓCICA POLISSACÁRIDA, 23-VALENTE, MONODOSE, SOLUÇÃO INJETÁVEL, C/ SISTEMA DE PREPARO E APLICAÇÃO",
          "Nº processo": 30261,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 6,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmB2ypdGjBY/20aa420b24a94c3392722d671b2c0641250bc648?#tarefa-142661"
     },
     {
          "CATMAT": "449377",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "ETEPLIRSEN 50MG/ML, SOLUÇÃO INJETÁVEL C/ 2ML",
          "Nº processo": 30263,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8Gn1PzBDrQ/c20554648beb17406d51975307a17b1678cfc892?#tarefa-137802"
     },
     {
          "CATMAT": "460722",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "NITISINONA 20MG",
          "Nº processo": 30264,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 32,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56gNpEwAbv/2fd5a89a5f40bbc06d8b2cfe4df4d84f655bd080?#tarefa-107017"
     },
     {
          "CATMAT": "0461553",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BIOPESTICIDA, A BASE DE BACILLUS SP., POTÊNCIA ATÉ 5.000 UTI, SÓLIDO",
          "Nº processo": 30266,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": " Validação TR",
          "Status atual": "Nenhum",
          "Status geral": [
               13,
               1,
               0
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73k0P4blB0/b92494fe4f75b137539c7ed11a097249ca90e227?#tarefa-141503"
     },
     {
          "CATMAT": "458859",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BUROSUMABE, 20 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30267,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               10,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8GdVPzBDrQ/c3a9bbc7a8110b30e13e67e4c98cc05c019cc37d?#tarefa-108581"
     },
     {
          "CATMAT": "394865",
          "Depto.": "Assessoria/DICONF/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ÁCIDO TIÓCTICO 600MG",
          "Nº processo": 30268,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56jEpEwAbv/1e1602ab42eb15e57339279ba0956d561de661ad?#tarefa-108624"
     },
     {
          "CATMAT": "267765",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "SULFADIAZINA 500 MG",
          "Nº processo": 30269,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBmrpdGjBY/95efe3c9c30464f7b4d3e6cb05a5fd9c96d4e6e8?#tarefa-108713"
     },
     {
          "CATMAT": "272737",
          "Depto.": "Analista/CGAFME/DAF/SCTIE/MS",
          "Insumo": "MEGLUMINA ANTIMONIATO, 300 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30270,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73gJP4blB0/e3d50b8113ce57c96978dfe3e411b9ac18d2b64e?#tarefa-109065"
     },
     {
          "CATMAT": "375707",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "CONJUNTO PARA ANÁLISE, QUALITATIVO DE RNA POR RT-PCR, TAQ DNA POLIMERASE RECOMBINANTE COM ANTICORPO, TRANSCRIPTASE REVERSA COM BAIXA ATIVIDADE RNASE H, TAMPÃO DE REAÇÃO 2X, MGS04 5MM",
          "Nº processo": 30272,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8GDlPzBDrQ/ec457fb85cfa27c19bbcab679a11e37c78b2b285?#tarefa-109861"
     },
     {
          "CATMAT": "381776",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ESTIRIPENTOL",
          "Nº processo": 30273,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 21,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56WJpEwAbv/91c2a7d4b7640b544abdfec89e45940f85e8f760?#tarefa-110398"
     },
     {
          "CATMAT": "272831",
          "Depto.": "Diretor/DLOG/SE/MS",
          "Insumo": "QUETIAPINA, 25 MG",
          "Nº processo": 30274,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBWvpdGjBY/a96b6b8154a2efb16a2fdefdc3e06197038f0642?#tarefa-110832"
     },
     {
          "CATMAT": "368694",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "ECULIZUMABE 10MG/ML, INJETÁVEL",
          "Nº processo": 30275,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 36,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDrZRoyAZz/c9cea7144493dbc4bf3196c66d2c69f4102d8cdc?#tarefa-111280"
     },
     {
          "CATMAT": "433437",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ACESSÓRIO BOMBA INSULINA, CONJUNTO DE INFUSÃO, POLÍMERO, CATÉTER CERCA 60CM C/ CONECTOR LUER LOCK, C/ CÂNULA INFUSÃO CERCA 8MM, SUPORTE, TAMPA E ADESIVO PROTETOR, P/ INSERÇÃO C/ APLICADOR",
          "Nº processo": 30277,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73XwP4blB0/1cb63c5f0b3d5f871a5be5755990b5a582c53488?#tarefa-112579"
     },
     {
          "CATMAT": "448290",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IVACAFTOR 150MG",
          "Nº processo": 30281,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyD9zRoyAZz/a77362ac37d0547cc00370b7f85d566946a6b42d?#tarefa-115042"
     },
     {
          "CATMAT": "356705",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "MERCAPTAMINA, COMPOSIÇÃO: SAL BITARTARATO, CONCENTRAÇÃO: 50MG",
          "Nº processo": 30282,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8GoKPzBDrQ/d05f6fa0084b4e7b6c72712968666c4bbed3b9b8?#tarefa-115082"
     },
     {
          "CATMAT": "362720",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BISOPROLOL FUMARATO 2,5MG",
          "Nº processo": 30283,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmB6GpdGjBY/bad2e541b74c2bb9a993771d7a1cba6f75fd641d?#tarefa-115379"
     },
     {
          "CATMAT": "453069",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "VACINA, MENINGOCÓCICA ACWY, CONJUGADA, INJETÁVEL",
          "Nº processo": 30284,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 18,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73x5P4blB0/679ce40c0bec23b254032b455321a81d218ef078?#tarefa-141365"
     },
     {
          "CATMAT": "315088",
          "Depto.": "Coordenador/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ENTECAVIR, 0,5 MG",
          "Nº processo": 30285,
          "CIM.": "",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8G6QPzBDrQ/9c1aaedb38af955f15100d347c67104392867286?#tarefa-143678"
     },
     {
          "CATMAT": "417568",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ÁCIDO QUENODESOXICÓLICO",
          "Nº processo": 30286,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 11,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73AOP4blB0/ff9db151498583af55107f60cca2fa406149a7c8?#tarefa-117139"
     },
     {
          "CATMAT": "292208",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "RIFAMPICINA, 20 MG/ML, SUSPENSÃO ORAL",
          "Nº processo": 30287,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyDx6RoyAZz/c2b43d3ba1906cc554535e005f474115c65e593f?#tarefa-134458"
     },
     {
          "CATMAT": "368694",
          "Depto.": "Analista/DIVAN/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ECULIZUMABE, CONCENTRAÇÃO 10, FORMA FARMACÊUTICA SOLUÇÃO INJETÁVEL",
          "Nº processo": 30288,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Publicação",
          "Status atual": "Empenho",
          "Status geral": [
               13,
               9,
               11
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8GJ9PzBDrQ/1d3955a55ff5b09ec5d57cf5603df2e8412ee5de?#tarefa-117263"
     },
     {
          "CATMAT": "268829",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TERIZIDONA, 250 MG",
          "Nº processo": 30289,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq563GpEwAbv/7b69ecf39e83b7e5f702358c11806d875b4e4e7d?#tarefa-140819"
     },
     {
          "CATMAT": "436347",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IDURSULFASE, COMPOSIÇÃO BETA, CONCENTRAÇÃO 2, FORMA FARMACÊUTICA SOLUÇÃO P/ INFUSÃO VENOSA",
          "Nº processo": 30290,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Pregão CGJUD",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBDNpdGjBY/8cb7ab21545eeda6a4b8c73a4f2271cc63294895?#tarefa-117719"
     },
     {
          "CATMAT": "353332",
          "Depto.": "Analista/CGAHV/DCCI/SVS/MS",
          "Insumo": "RALTEGRAVIR, 400 MG",
          "Nº processo": 30291,
          "CIM.": "",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73aGP4blB0/4f0f10a8a270f6f74609ddbf415c96bc6495596f?#tarefa-117530"
     },
     {
          "CATMAT": "433686",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "MERCAPTAMINA 25MG",
          "Nº processo": 30293,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": "Negociação",
          "Status atual": "Doc. Téc.e Fiscal",
          "Status geral": [
               13,
               2,
               3
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56N2pEwAbv/6de2f190373d52cfc909ca6922da78cb27ce2211?#tarefa-138482"
     },
     {
          "CATMAT": "435926",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "DIETA INFANTIL, LÍQUIDO, ENTERAL OU ORAL, PTN ISOLADA SOJA, ISENTO CARBOIDRATO, ÓLEOS VEGETAIS, AA'S, VIT., MINERAIS, ISENTO GLÚTEN, LACTOSE, C/ LECITINA SOJA, S/ SABOR",
          "Nº processo": 30295,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 14,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP734BP4blB0/c488efe2ea407335f8951e08ffb180a48157efaf?#tarefa-140200"
     },
     {
          "CATMAT": "388383",
          "Depto.": "Analista/CGCEAF/DAF/SCTIE/MS, Coordenador Geral/CGCEAF/DAF/SCTIE/MS",
          "Insumo": "TOCILIZUMABE, 20 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30296,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyD0bRoyAZz/f4a70d81fe8898efe31901a5362fa9ce2ba8f36c?#tarefa-118365"
     },
     {
          "CATMAT": "384385",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO÷, CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUALITATIVO M.TUBERCULOSIS RESISTÊNCIA RIFAMPICINA, PCR TEMPO REAL, TESTE",
          "Nº processo": 30297,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8G50PzBDrQ/aaacc92d54dfda0f03847f074773a063b27f8389?#tarefa-141339"
     },
     {
          "CATMAT": "268861",
          "Depto.": "Analista/CGAFME/DAF/SCTIE/MS",
          "Insumo": "ITRACONAZOL, 100 MG",
          "Nº processo": 30298,
          "CIM.": "",
          "Área demandante": "CGAFME/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBJwpdGjBY/111ba44c29bb9447cdddd1721203c3c47dd90c56?#tarefa-142278"
     },
     {
          "CATMAT": "358753",
          "Depto.": "Coordenador Geral/CGAFB/DAF/SCTIE/MS",
          "Insumo": "MISOPROSTOL, 25 MCG, COMPRIMIDO VAGINAL",
          "Nº processo": 30300,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq56V4pEwAbv/f54e563233760aec9ff4c1ad9050b174c6f603da?#tarefa-143462"
     },
     {
          "CATMAT": "448290",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IVACAFTOR 150MG",
          "Nº processo": 30301,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmBnypdGjBY/9f7901d812c69a23748638aec12c9bfd130c4b5e?#tarefa-119344"
     },
     {
          "CATMAT": "412776",
          "Depto.": "Coordenador/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ACETATO DE ABIRATERONA 250MG",
          "Nº processo": 30302,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP73Y9P4blB0/896989bbb657916ec45da3b192e53fa0b64820a7?#tarefa-119525"
     },
     {
          "CATMAT": "268956",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS",
          "Insumo": "LEVONORGESTREL, 0,75 MG",
          "Nº processo": 30303,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR8Gr1PzBDrQ/0a98a0c552a2c08e81ceaadf61f016bdf7931716?#tarefa-120071"
     },
     {
          "CATMAT": "398702",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS",
          "Insumo": "MEDROXIPROGESTERONA ACETATO, 150 MG/ML, SUSPENSÃO INJETÁVEL",
          "Nº processo": 30304,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR89ZVqzBDrQ/760520943594e0ef8b4d3e55caa1c604509eb376?#tarefa-120291"
     },
     {
          "CATMAT": "335447",
          "Depto.": "Analista/CGLAB/DAEVS/SVS/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO PARA AUTOMAÇÃO, QUALITATIVO DE ROTAVÍRUS, ELISA, TESTE, EM AMOSTRAS FECAIS",
          "Nº processo": 30305,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5L7EREwAbv/9d1f094ac5e2953f66f3ff0c0eaefc71f8d4e252?#tarefa-120440"
     },
     {
          "CATMAT": "458859",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BUROSUMABE, 20 MG/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30306,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmX3rRdGjBY/a93c8e53a6835a75b8ba7b3643cee8194aa04434?#tarefa-120483"
     },
     {
          "CATMAT": "434617",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 14-24%, USO ORAL, SERINGA PREENCHIDA",
          "Nº processo": 30307,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyz57poyAZz/f0f9ea428aff87db2876ae48366c5f94da660725?#tarefa-140779"
     },
     {
          "CATMAT": "454936",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 50MG/ML, USO ORAL, ISENTO DE THC; 120ML",
          "Nº processo": 30308,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR89elqzBDrQ/929e5cb1889576da568220a81c6a3743ddb4fb1d?#tarefa-140201"
     },
     {
          "CATMAT": "434617",
          "Depto.": "Analista/CGJUD, Coordenador Geral/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 14-24%, USO ORAL, SERINGA PREENCHIDA",
          "Nº processo": 30309,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5LEJREwAbv/c32600737426d90e5a654934c87bc3aa31563db7?#tarefa-121002"
     },
     {
          "CATMAT": "434617",
          "Depto.": "Analista/CGJUD",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 14-24%, USO ORAL, SERINGA PREENCHIDA",
          "Nº processo": 30310,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/mrA2kPvy1jpQy47x/c23111cc3f15e8c7a1fb0a92fa66adf449d40f90?#tarefa-139888"
     },
     {
          "CATMAT": "267733",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS",
          "Insumo": "NORETISTERONA, 0,35 MG, BLISTER CALENDÁRIO COM 35 UNIDADES",
          "Nº processo": 30311,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXjvRdGjBY/06dfd8fc1926612645828bf0cf97846d8070bccc?#tarefa-121251"
     },
     {
          "CATMAT": "267733",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS",
          "Insumo": "NORETISTERONA, 0,35 MG, BLISTER CALENDÁRIO COM 35 UNIDADES",
          "Nº processo": 30311,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXjvRdGjBY/06dfd8fc1926612645828bf0cf97846d8070bccc?#tarefa-121251"
     },
     {
          "CATMAT": "437083",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "SECUQUINUMABE, 150 MG/ML, SOLUÇÃO INJETÁVEL, COM CANETA APLICADORA",
          "Nº processo": 30313,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyz2ZpoyAZz/f544649203da1f11f61a1504279d42eb0ae5cd1f?#tarefa-121466"
     },
     {
          "CATMAT": "449650",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "EXTRATO MEDICINAL, ÓLEO DE CANABIDIOL, 50 MG/ML, SOLUÇÃO ORAL - GOTAS",
          "Nº processo": 30314,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR892yqzBDrQ/42a623345fb7b4e2d3afdf1cddaf2ff090ec2068?#tarefa-140202"
     },
     {
          "CATMAT": "380137",
          "Depto.": "Analista/DEIDT/SVS/MS, Diretor/DEIDT/SVS/MS",
          "Insumo": "ALFACIPERMETRINA, 20% P/V, SUSPENSÃO CONCENTRADA, CAS 67375-30-8",
          "Nº processo": 30315,
          "CIM.": "",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 13,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5LlYREwAbv/8c50d64e565aad4d28fb916d6823601cf32bbd18?#tarefa-122361"
     },
     {
          "CATMAT": "442646",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "HORMÔNIO PARATIREÓIDE, PTH, 50MCG, PÓ LIÓFILO P/ INJETAVEL + DILUENTE, C/ SISTEMA DE PREPARO",
          "Nº processo": 30316,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXejRdGjBY/f6e935a61fad7435022658c19216f38926c5d614?#tarefa-122499"
     },
     {
          "CATMAT": "424099",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "MERCAPTAMINA, 5,5 MG/ML, SOLUÇÃO OFTÁLMICA C/5ML",
          "Nº processo": 30317,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyzA4poyAZz/fbae431a08946e953812523f351ac8d21b9f7338?#tarefa-123178"
     },
     {
          "CATMAT": "414879",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "TOCOFERSONALA, 50MG/ML, SOLUÇÃO ORAL C/60ML",
          "Nº processo": 30318,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/Orml2qLNn3P1z5Xo/d29ec3f5736126d943ca3e71aea73878ad7146d7?#tarefa-123206"
     },
     {
          "CATMAT": "464164",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "SELEXIPAGUE 1,6 MG",
          "Nº processo": 30319,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 5,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR89ydqzBDrQ/f5888fb282d6d0d8c8795db05e2c227c2e846a68?#tarefa-123008"
     },
     {
          "CATMAT": "311390",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "BEVACIZUMABE, CONCENTRAÇÃO: 25 MG,ML, FORMA FARMACEUTICA: SOLUÇÃO INJETÁVEL",
          "Nº processo": 30320,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/OoLd3PaLEgpnGg68/6bdbefe6921fc4de57895650b01121d1fd4aab2a?#tarefa-123291"
     },
     {
          "CATMAT": "402930",
          "Depto.": "Coordenador Geral/CGLAB/DAEVS/SVS/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO÷, CONJUNTO COMPLETO, QUALITATIVO ANTI LEPTOSPIRA INTERROGANS IGM, ELISA, TESTE",
          "Nº processo": 30321,
          "CIM.": "",
          "Área demandante": "CGLAB/DAEVS/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/Lvyw8qz9z4RANY5m/8063b4f43b111ad52b55493ce057c183486f2841?#tarefa-123427"
     },
     {
          "CATMAT": "439908",
          "Depto.": "Analista/CGAFB/DAF/SCTIE/MS, Coordenador Geral/CGAFB/DAF/SCTIE/MS",
          "Insumo": "AGULHA, AÇO INOXIDÁVEL, P/ CANETA APLICADORA, CERCA DE 32G X 4 MM, CONECTOR LUER LOCK OU SLIP, PRETETOR C/ LACRE, DESCARTÁVEL, ESTÉRIL",
          "Nº processo": 30322,
          "CIM.": "",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 10,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5LvQREwAbv/34031d01306c5ee9e8d22aee56639ae576432ef0?#tarefa-142779"
     },
     {
          "CATMAT": "425497",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "ESTERASE, COMPOSIÇÃO: INIBIDOR DE ESTERASE C1 HUMANA, CONCENTRAÇÃO: 500 UI, FORMA FARMACÊUTICA : PÓ LIÓFILO PARA INJETÁVEL",
          "Nº processo": 30323,
          "CIM.": "",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Pregão CGJUD",
          "Dias Corridos (Ult. Tarefa)": 7,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXO2RdGjBY/4c137586b8a024fb03a809de81f53da0ad75395e?#tarefa-123869"
     },
     {
          "CATMAT": "452607",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "GLATIRÂMER ACETATO, 40 MG/ML, SOLUÇÃO INJETÁVEL, EM SERINGA PREENCHIDA C/ 1ML",
          "Nº processo": 30324,
          "CIM.": "",
          "Área demandante": "CGCEAF/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/D8VWwP7zNVp4blB0/1946026b2a9e58e8f33981a5c971a38cb7edd38e?#tarefa-124040"
     },
     {
          "CATMAT": "361446",
          "Depto.": "Secretário/SE/MS",
          "Insumo": "REAGENTE PARA DIAGNÓSTICO CLÍNICO., CONJUNTO COMPLETO, QUALITATIVO DE ANTICORPOS ANTI-TREPONEMA PALLIDUM, IMUNOCROMATOGRAFIA, TESTE",
          "Nº processo": 30325,
          "CIM.": "Não",
          "Área demandante": "CGAHV/DCCI/SVS/MS",
          "Risco": "",
          "Modalidade": "Pregão",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyzGzpoyAZz/f3a8cf08270a5434b0f6aa6246488a8438d7c9ed?#tarefa-140961"
     },
     {
          "CATMAT": "391660",
          "Depto.": "Secretário/SVS/MS",
          "Insumo": "VACINA HUMANA, PNEUMOCÓCICA CONJUGADA 13-VALENTE, SOROTIPOS: 4,6B,9V,14,18C,19F,23F,1,3,5,6A,7F,19A, SUSPENSÃO INJETÁVEL",
          "Nº processo": 30326,
          "CIM.": "Não",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Inexigibilidade",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXBGRdGjBY/44ca6b725a7a9df7034ada16e4b9e1d0482379f8?#tarefa-140465"
     },
     {
          "CATMAT": "383436",
          "Depto.": "Analista/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "AZACITIDINA, 100 MG, PÓ LIÓFILO P/ INJETÁVEL",
          "Nº processo": 30328,
          "CIM.": "Não",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/BegdXqyzzrpoyAZz/35f5cbd9ac2f2c5c012990750e8c23426014477a?#tarefa-126283"
     },
     {
          "CATMAT": "271157",
          "Depto.": "Diretor/DAF/SCTIE/MS",
          "Insumo": "INSULINA, HUMANA, NPH, 100U/ML, INJETÁVEL",
          "Nº processo": 30329,
          "CIM.": "Não",
          "Área demandante": "CGAFB/DAF/SCTIE/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 4,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d3LJWR899QqzBDrQ/3d8d4238263c73d073440a115682061e0ec975c4?#tarefa-139081"
     },
     {
          "CATMAT": "405901",
          "Depto.": "Secretário/SVS/MS",
          "Insumo": "SORO, ANTI-RÁBICO, IMUNOGLOBULINA EQÜINA + IMUNOGLOBULINA HUMANA, 200 UI/ML, SOLUÇÃO INJETÁVEL",
          "Nº processo": 30330,
          "CIM.": "Não",
          "Área demandante": "Aquisição/DEIDT/SVS/MS",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 3,
          "Status anterior": "Nenhum",
          "Status atual": " Validação TR",
          "Status geral": [
               13,
               0,
               1
          ],
          "href": "http://localhost:8089/processos/visualizar/d9ljXq5LZaREwAbv/584dedde2c0f46427796ddf5ff89b3f0037f8bc0?#tarefa-142919"
     },
     {
          "CATMAT": "450161",
          "Depto.": "Analista/DIVIP/COLMER/CGIES/DLOG/SE/MS",
          "Insumo": "IVACAFTOR, ASSOCIADO AO LUMACAFTOR 125MG + 200MG",
          "Nº processo": 30331,
          "CIM.": "Não",
          "Área demandante": "CGJUD",
          "Risco": "",
          "Modalidade": "Dispensa",
          "Dias Corridos (Ult. Tarefa)": 0,
          "Status anterior": " Validação TR",
          "Status atual": "Negociação",
          "Status geral": [
               13,
               1,
               2
          ],
          "href": "http://localhost:8089/processos/visualizar/3OJomqmXVgRdGjBY/0dfd3cd8fa5d53660f787f2b5a48eecaefde5ab0?#tarefa-127357"
     }
]