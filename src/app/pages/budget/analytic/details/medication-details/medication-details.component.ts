import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, NavigationEnd } from '@angular/router';
import { AnalyticService } from '../../../../../services/analytic/analytic.service';
import { ChartComponent } from '../../../../../components/chart/chart.component';

import {
  sinproc_json,
  chartExample1,
} from "../../../../../variables/charts";
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'medication-details',
  templateUrl: './medication-details.component.html',
  styleUrls: ['./medication-details.component.scss']
})

export class MedicationDetailsComponent implements OnInit, AfterViewInit {
  @ViewChild('chartMedication') chartMedication: ChartComponent;

  headerData: Object[];
  analyticData: Object[];
  tableTitle: String;
  tableOption: String;
  tableAggregator: String;
  tableDetail: String;
  tableView: number = 0;

  medicine: any;
  processStatus: any;

  translate_coords = {
    basic: "Básica",
    strategic: "Estratégica",
    specialized: "Especializada",
    farmpop: "Farmácia Popular"
  }

  constructor(
    private _Activatedroute: ActivatedRoute,
    private analyticService: AnalyticService,
    private router: Router,
  ) { 
    
  }

  ngOnInit() {
    this._Activatedroute.paramMap.subscribe((params : ParamMap)=> { 
      this.tableOption = params.get('coord');
      this.tableView = Number(params.get('view'));
      this.tableAggregator = params.get('aggreg');
      this.tableDetail = params.get('detail');      
      this.headerData = [];
      this.analyticData = [];

      
      // @ts-ignore
      this.analyticService.getMedicine(this.tableDetail, JSON.parse(window.localStorage.getItem("dataTable"))["Nº processo"]).then((p) => {
        this.medicine = p;
        this.processStatus = sinproc_json.filter(p1 => p1.CATMAT === this.tableDetail);
        this.processStatus[0]["Risco"] = p["risco"];
        this.processStatus[0]["CIM."] = p["cim"];
        console.log("==========>", this.processStatus, p);
      });
      
      // setTimeout(this.setupHistoryChart.bind(this), 5000); 

      // this.updateData();
    });
  }

  ngAfterViewInit() {
    this.setupHistoryChart();
  }

  updateData() {
    this.analyticService.getTable(this.tableOption, this.tableView, this.tableAggregator, this.tableDetail, "").then((data: any) => {
      if(data.length == 0) return
      this.headerData = Object.keys(data[0]);
      this.analyticData = data;
    });
  }

  setupHistoryChart() {
    let gMedicationConfig = {
      name : "",
      data : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      dataType : "dataType",
      type : "line",
      labels : ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
      scales : chartExample1.options.scales,
      colors:{
        background : ["#c45850"],
        border : ["#c45850"]
      } 
    }

    console.log(this.chartMedication);
    
    this.chartMedication.chartData = gMedicationConfig;
    this.chartMedication.config();
    this.chartMedication.addDataset([{
      // backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
      label: "Aquisições",
      data: [9, 7, 3, 5, 2, 10, 15, 16, 19, 3, 1, 9]
    }],["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]);
  }

  setupProcessStatus() {
    
  }
}
