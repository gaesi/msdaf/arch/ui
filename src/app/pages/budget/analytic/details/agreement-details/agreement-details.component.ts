import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, NavigationEnd } from '@angular/router';
import { ChartData, ChartComponent } from '../../../../../components/chart/chart.component';
// import { AnalyticService } from '../../../../../services/analytic/analytic.service';


@Component({
  selector: 'agreement-details',
  templateUrl: './agreement-details.component.html',
  styleUrls: ['./agreement-details.component.scss']
})

export class AgreementDetailsComponent implements OnInit {

  headerData: Object[];
  analyticData: Object[];
  tableTitle: String;
  tableOption: String;
  tableAggregator: String;
  tableDetail: String;
  tableView: number = 0;
  //chart
  chartData: ChartData = {
    name: "chart component",
    type: "line",
    labels: ["Jan", "Feb", "March", "April", "May", "June","July","Aug","Sep","Oct","Nov","Dec"],
    dataType: 'tipo dos dados',
    data: [9,7 , 3, 5, 2, 10,15,16,19,3,1,9],
    scales: {},
    colors: {
      background: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      border: 'red',
    }
  };

  translate_coords = {
    basic: "Básica",
    strategic: "Estratégica",
    specialized: "Especializada",
    farmpop: "Farmácia Popular"
  }

  constructor(
    private _Activatedroute: ActivatedRoute,
    // private analyticService: AnalyticService,
    private router: Router,
  ) { 
    
  }

  ngOnInit() {
    this._Activatedroute.paramMap.subscribe((params : ParamMap)=> { 
      this.tableOption = params.get('coord');
      this.tableView = Number(params.get('view'));
      this.tableAggregator = params.get('aggreg');
      this.tableDetail = params.get('detail');      
      this.headerData = ['header1', 'header2', 'header3', 'header4'];
      this.analyticData = [
        {
          header1: 'asdkfkads',
          header2: 'agaertsdfsa',
          header3: 'asdgheweqwew',
          header4: 'jklçouijklçuiop'
        },
        {
          header1: 'asdkfkads',
          header2: 'agaertsdfsa',
          header3: 'asdgheweqwew',
          header4: 'jklçouijklçuiop'
        }
      ];
      // this.updateData();
    });
  }

  // updateData() {
  //   this.analyticService.getTable(this.tableOption, this.tableView, this.tableAggregator, this.tableDetail, "").then((data: any) => {
  //     this.headerData = Object.keys(data[0]);
  //     this.analyticData = data;
  //     console.log(Object.keys(data[0]), data);
  //   });
  // }

}
