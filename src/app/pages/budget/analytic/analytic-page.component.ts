import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, NavigationEnd } from '@angular/router';
import { AnalyticService } from '../../../services/analytic/analytic.service';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'ngbd-table-complete',
  templateUrl: './analytic-page.component.html',
  styleUrls: ['./analytic-page.component.scss'],
  providers:[DecimalPipe]
})

export class AnalyticPage implements OnInit {
  headerData: Object[];
  analyticData: Object[];
  tableTitle: String;
  tableOption: String;
  tableAggregator: String;
  tableDetail: String;
  tableView: number = 0;
  tableSubView: number = 0;
  viewType: any;

  constructor(
    private _Activatedroute: ActivatedRoute,
    private analyticService: AnalyticService,
    private router: Router
    // pipe: DecimalPipe,
  )
  {
    // router.events.pipe(
    //   filter(event => event instanceof NavigationEnd)
    // ).subscribe((event: NavigationEnd) => {
    //   this.updateData( this.infoId );
    // });
  }

  ngOnInit(){
    this._Activatedroute.paramMap.subscribe((params : ParamMap)=> { 
      this.tableOption = params.get('coord') || 'basic';
      this.tableView = Number(params.get('view')) || 0;
      this.tableAggregator = params.get('aggreg');
      this.tableDetail = params.get('detail');
      this.headerData = [];
      this.analyticData = [];
      this.updateData();
    }); 
  }
  
  updateView(option: String) {
    if(this.tableDetail){
      for(let i = 0; i < this.viewType.length; i++){
        if(this.viewType[i].type.includes(option)){
          this.tableSubView = i;
          this.updateData();
          return;
        }
      }  
    }
    else{
      for(let i = 0; i < this.viewType.length; i++){
        if(this.viewType[i].type.includes(option)){
          this.tableView = i;
          this.updateData();
          return;
        }
      }
    }
  }
  
  updateData() {
    let configJSON = this.analyticService.configJSON({
      coord: this.tableOption,
      view: this.tableView,
      detail: this.tableDetail
    });
    console.log("configJSON: ", configJSON);
    this.tableTitle = configJSON["tableTitle"];
    this.viewType = configJSON["viewType"];
    this.analyticService.getTable(this.tableOption,this.tableView,this.tableAggregator, this.tableDetail,this.tableSubView)
      .then((data: any) => {
        this.headerData = Object.keys(data[0]);
        this.analyticData = data;
        console.log("analytic page", this.analyticData);
        

        if(!data) this.headerData = ["Sem dados para exibir"]

        if(this.tableDetail){
          // @ts-ignore
          $(`.pill-${this.tableSubView}`).click();
        }else{
          // @ts-ignore
          $(`.pill-${this.tableView}`).click();
        }
        if(this.tableAggregator && !this.tableDetail){
          // @ts-ignore
          $(`.nav-pills`).hide();
        }
        else{
          // @ts-ignore
          $(`.nav-pills`).show();
        }
      });
    }
}
