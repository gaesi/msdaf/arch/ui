import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoaderComponent } from '../../components/loader/loader.component';

import {
  medicines
} from "../../variables/resulttbl";


@Injectable({
  providedIn: 'root'
})
export class StockService {

  dataJSON: any;
  public urlAPI = `http://magbudget.herokuapp.com/magv2/stock`;

  constructor(private http: HttpClient, public loader: LoaderComponent) {

  }

  getStock(page, qtd) {
    let url = `${this.urlAPI}/${page}/${qtd}`
    return new Promise(resolve => {
      this.http.get(url).subscribe(response => {
        resolve(this.parseMetadata(response));
        setTimeout( () => { this.loader.stop(); }, 1000);
      });
    });
  }

  getStockSearch(page, qtd, field, search){
    let url = `${this.urlAPI}/${page}/${qtd}`
    if (search) url = `${this.urlAPI}/${page}/${qtd}/${field}/${this.utf8ToB64(search)}`;

    return new Promise(resolve => {
      this.http.get(url).subscribe(response => {
        // console.log(response);
        resolve(this.parseMetadata(response));
        setTimeout( () => { this.loader.stop(); }, 1000);
        
      });
    });
  }

  parseMetadata(response) {
    const total = response.metadados.total;
    delete response.metadados.total;
    const m = response.metadados;
    const keys = {
      'Almoxarifado': 'almox',
      'CATMAT': 'catmat',
      'Código Interno': 'codigo',
      'Município': 'municipio',
      'Origem do Dado': 'sistema',
      'Produto': 'produto',
      'Qtd. Estoque': 'estoque',
      'UF': 'estado',
      'IBGE': 'ibge'
    };
    const fields = Object.keys(m).map(k => ({ label: k, type: m[k], key: keys[k], aggregations: [], searchable: ['varchar', 'number'].includes(m[k]) }));
    fields.push({ label: 'Progresso', type: 'progress', key: 'progress', aggregations: [], searchable: false });
    response.metadata = { fields, total };
    response.data.forEach((d) => {
      d['Progresso'] = { 'Status geral': [ 10, 8, 3], 'Status atual': 'Iniciado', 'Status anterior': 'parado' };
    });
    return response;
  }

  utf8ToB64(str) {
    return btoa(unescape(encodeURIComponent(str)));
  }
}
