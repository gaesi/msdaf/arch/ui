import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, NavigationEnd } from '@angular/router';
import { AnalyticService } from '../../services/analytic/analytic.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})

export class DataTableComponent implements OnInit, OnChanges {
  
  @Input('tableData') tableData: any;
  @Input('isLinkable') isLinkable: boolean;
  @Input('componentType') componentType: any;

  @Output() updateTableEmit = new EventEmitter<Object>();
  @Output() searchEmit = new EventEmitter<Object>();

  headerData = [];
  analyticData = [];

  
  tableDetail: String;
  tableAggregator: String;
  tableView: number = 0;
  tableOption: String;
  goSinproc: any;
  statusProc: any;
  
  ipage = 1;
  npages: any;
  nItems = 10; // items per page

  fieldOptions = [];
  fieldSearch: any;

  sortedData:any;

  constructor(
    private _Activatedroute: ActivatedRoute,
    private analyticService: AnalyticService
  ){}

  ngOnInit() {
    this.renderTable(this.tableData);
    this.subscribeRouter();
  } 

  renderTable(tableData: any){
    console.log(tableData);
    if (!tableData.data) return;

    this.npages = this.generateTotalPages(tableData.metadata.total);

    [ this.headerData, this.analyticData, this.fieldOptions ] = [ ...this.extractData(tableData) ];

    // In case the lines of tables need to show progress bar
    this.setupProgressData();
  }

  subscribeRouter() {
    this._Activatedroute.paramMap.subscribe((params : ParamMap)=> { 
      this.tableOption = params.get('coord');
      this.tableDetail = params.get('detail');
      this.tableAggregator = params.get('aggreg');
      this.tableView = Number(params.get('view'));
    });
  }

  generateTotalPages(total: number) {
    return Math.ceil(total/this.nItems);
  }

  extractData(tableData) {
    return [
      tableData.metadata.fields,
      tableData.data,
      tableData.metadata.fields.filter(f => f.searchable)
    ];
  }

  setupProgressData() {
    if(this.componentType == "progress"){
      this.statusProc = this.analyticData.map((obj) => {
        return {
          "Status anterior": obj["Status anterior"],
          "Status atual": obj["Status atual"],
          "Status geral": obj["Status geral"]
        }
      });
      
      this.headerData.splice(this.headerData.indexOf("Status anterior"));
      this.analyticData = this.analyticData.map((obj) => {
        delete obj['Status anterior'];
        delete obj['Status atual'];
        delete obj['Status geral'];
        return obj;
      });
      // console.log("----->", this.statusProc[0]["Status geral"].length);
    }
  }

  createLink(data: any) {

    // console.log(data);
    // console.log(this.tableDetail);
  
    if(!Object.values(data)[0]) return``
    if(this.tableDetail && !(window.location.pathname.split("/").length > 3)) return
    
    if((window.location.pathname.split("/").length > 3)){
      this.goSinproc = true;
      return data["href"];
    }
    
    let info: string;
    info = Object.values(data)[0].toString();
    
    return `${info}`;
  }

  processProgBar(gsIndex, lsIndex, csIndex) {
    this.analyticData.map(() => {
      this.analyticData[gsIndex],
      this.analyticData[lsIndex],
      this.analyticData[csIndex]
    });
  }

  setStorage(data){
    // @ts-ignore
    window.localStorage.setItem("dataTable", JSON.stringify(data));
  }


  //  Filter component methods
  getSortedData(msg){
    this.sortedData = msg;
    switch(this.sortedData.type) {
      case 'range':
        this.analyticData = this.sortNumber(this.sortedData.header);
        if(this.sortedData.order == 'Descendente') this.analyticData = this.analyticData.reverse();
        // if(this.sortedData.intervalStarts && this.sortedData.intervalEnds) this.analyticData = this.trimArr(this.sortedData);
        break;
      
      case 'date':
        this.analyticData = this.sortDate(this.sortedData.header);
        if(this.sortedData.order == 'Descendente') this.analyticData = this.analyticData.reverse();
        break;
      
      case 'text':
        this.analyticData = this.sortText(this.sortedData.header);
        if(this.sortedData.order == 'Descendente') this.analyticData = this.analyticData.reverse();
        break;  

      default:
        console.info('filter mode default is undefined');
    }
  }

  sortNumber(key) {
    var numbers = this.tableData.data.slice(0);
    numbers.sort(function(a,b) {
        return a[key] - b[key];
    });
    return numbers;
  }

  sortDate(key) {
    var byDate = this.tableData.data.slice(0);
    byDate.sort(function(a,b) {
        return Date.parse(a[key]) - Date.parse(b[key]);
    });
    return byDate;
  }

  sortText(key){
    var byText = this.tableData.data.slice(0);
    byText.sort(function(a,b) {
      var x = a[key].toLowerCase();
      var y = b[key].toLowerCase();
      return x < y ? -1 : x > y ? 1 : 0;
    });
    return byText;
  }

  // trimArr(param) {
  //   return [];
  // }


  // Pagination methods
  previous(){
    if (this.ipage > 1){
      this.ipage = this.ipage - 1;
      
      this.updateTableEmit.emit({
        page: this.ipage,
        qtd: this.nItems
      }); 
    }
  }
  
  next(){
    if (this.ipage < this.npages){
      this.ipage = this.ipage + 1;

      this.updateTableEmit.emit({
        page: this.ipage,
        qtd: this.nItems
      }); 
    }
  }

  ngOnChanges(e){
    // this.tableData = e.tableData.currentValue;
    this.renderTable(e.tableData.currentValue);
  }

  searchByText(e) {
    this.searchEmit.emit({
      page: 1,
      qtd: this.nItems,
      search: e,
      field: this.fieldSearch
    }); 
  }

  setFieldSearch(e){
    this.fieldSearch = e.srcElement.value;
  }

  buildTableCell(data, type) {
    switch(type) {
      case 'varchar':
        return data;
      case 'number':
        return data;
      case 'progress':
        return '';
      case 'date':
        return `<strong>${data}</strong>`;
    }
  }
}
