import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    id?: string;
    path: string;
    title: string;
    icon: string;
    class: string;
    subItems?: [{}];
}
export const ROUTES: RouteInfo[] = [

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  // { id: 'dashboard', path: '/dashboard', title: 'Painel Orçamentário',  icon: 'fa fa-file-invoice-dollar fa-4x text-indigo', class: '', disabled: true},

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  //  { id: 'budget', path: '/budget', title: 'Visão Orçamentária',  icon: 'ni-money-coins text-teal', class: '', subItems: [
  //    { path: 'ploa', title: 'Visão Analítica', icon: 'ni-bullet-list-67 text-teal' },
  //    { path: 'basic', title: 'Básico (CGAFB)', icon: 'fa fa-stethoscope fa-4x text-danger' },
  //    { path: 'specialized', title: 'Especializado (CEAF)', icon: 'fa fa-pills fa-4x text-danger' },
  //    { path: 'strategic', title: 'Estratégico (CGAFME)', icon: 'fa fa-prescription-bottle-alt fa-4x text-danger' },
  //    { path: 'farmpop', title: 'FarmPop (CPFP)', icon: 'fa fa-clinic-medical fa-4x text-danger' }
  //  ]},

   { id: 'features', path: '/features', title: 'Novas Features',  icon: 'ni-money-coins text-teal', class: '', subItems: [
    { path: 'geodata', title: 'COVID19', icon: 'ni-bullet-list-67 text-teal' },
    { path: 'stock', title: 'Dados de Estoque', icon: 'fa fa-stethoscope fa-4x text-danger' },
  ]},

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  // { id: 'medicines', path: '/medicines', title: 'Medicamentos',  icon: 'fa fa-pills fa-4x text-danger', class: '', disabled: true},

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  // { id: 'transfers', path: '/transfers', title: 'Repasses',  icon: 'fa fa-dollar-sign fa-4x text-teal', class: '', disabled: true},

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  // { id: 'contracts', path: '/contracts', title: 'Contratos',  icon: 'fa fa-file-alt fa-4x text-blue', class: '', disabled: true},

  // @ts-ignore
  // tslint:disable-next-line:max-line-length
  { id: 'automation', path: '/automation', title: 'Automações',  icon: 'fa fa-users-cog text-yellow', class: '', subItems: [
    { path: 'notification', title: 'Notificação', icon: 'ni-bell-55 text-yellow' }, 
    { path: 'mulct', title: 'Ressarcimento', icon: 'ni-single-copy-04 text-yellow' },
    { path: 'material', title: 'Preditivo Físico', icon: 'ni-app text-yellow', disabled: true },
    { path: 'budget', title: 'Orçamentário', icon: 'ni-basket text-yellow', disabled: true },
  ]},

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

  isActiveRoute(item) {
    const section = item.id;
    return window.location.href.indexOf(section) >= 0;
  }

}
