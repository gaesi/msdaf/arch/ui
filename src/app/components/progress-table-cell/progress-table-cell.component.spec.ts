import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressTableCellComponent } from './progress-table-cell.component';

describe('ProgressTableCellComponent', () => {
  let component: ProgressTableCellComponent;
  let fixture: ComponentFixture<ProgressTableCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressTableCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressTableCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
