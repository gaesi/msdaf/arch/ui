import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-table-cell',
  templateUrl: './progress-table-cell.component.html',
  styleUrls: ['./progress-table-cell.component.scss']
})
export class ProgressTableCellComponent implements OnInit {

  @Input() cellData: any;

  constructor() {
   }

  ngOnInit() {
  }
}
